import axios from "axios";

const API_TOKEN = "tokenTest123";

export const AxiosInstance = axios.create({
  baseURL: "http://localhost:3000/",
  timeout: 1000,
  headers: { Authorization: API_TOKEN },
});
