import "./App.css";
import { Level1 } from "./level1/Level1";
import { Level2 } from "./level2/Level2";

function App() {
  return (
    <div className="App">
      <h1>Challenges</h1>
      <h2>Level1</h2>
      <Level1 shopId={5} />
      <h2>Level2</h2>
      <Level2 shopId={5} />
    </div>
  );
}

export default App;
