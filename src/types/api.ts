export type CardCombination = {
  value: number;
  cards: number[];
};

export type ApiCards = {
  equal?: CardCombination;
  floor?: CardCombination;
  ceil?: CardCombination;
};
