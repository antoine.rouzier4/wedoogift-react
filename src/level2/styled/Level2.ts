import styled from "styled-components";

export const InputContainer = styled("div")`
  margin-top: 10px;
`;

export const SubmitButtonContainer = styled("div")`
  margin-top: 10px;
`;
