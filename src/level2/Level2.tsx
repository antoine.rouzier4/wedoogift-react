import React, { FC, useState } from "react";
import { InputContainer, SubmitButtonContainer } from "./styled/Level2";
import { AxiosInstance } from "../constants/api";
import { ApiCards } from "../types/api";
import { ChooseAmount, DesiredAmount, NextAmount } from "./components/index";

interface Props {
  shopId: number;
}

export const Level2: FC<Props> = ({ shopId }) => {
  const [amount, setAmount] = useState<string>("");
  const [data, setData] = useState<ApiCards | undefined>(undefined);

  const handleSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
    AxiosInstance.get(`shop/${shopId}/search-combination`, {
      params: { amount },
    }).then((res) => {
      setData(res.data);
    });
  };

  const onAmountChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAmount(e.target.value);
  };

  const queryWithAmount = (newAmount: string) => {
    AxiosInstance.get(`shop/${shopId}/search-combination`, {
      params: { amount: newAmount },
    }).then((res) => {
      setAmount(newAmount);
      setData(res.data);
    });
  };

  const renderData = () => {
    if (data && data?.equal) {
      return <DesiredAmount cards={data.equal}></DesiredAmount>;
    }
    if (data?.ceil && data?.floor) {
      return (
        <ChooseAmount
          amount1={{
            amount: data.ceil,
            onPress: () => queryWithAmount(data?.ceil?.value?.toString() ?? ""),
          }}
          amount2={{
            amount: data.floor,
            onPress: () =>
              queryWithAmount(data?.floor?.value?.toString() ?? ""),
          }}
        ></ChooseAmount>
      );
    }
    const cardCombination = data?.ceil ?? data?.floor;
    if (!cardCombination) return;
    queryWithAmount(cardCombination.value.toString());
    return <></>;
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Montant désiré :</label>
        </div>
        <InputContainer>
          <input
            type="number"
            placeholder="60"
            name="amount"
            value={amount}
            onChange={onAmountChange}
          />
          {data && <NextAmount {...{ data, queryWithAmount }} />}
        </InputContainer>
        <SubmitButtonContainer>
          <input type="submit" value="Valider" />
        </SubmitButtonContainer>
      </form>
      {data && <>{renderData()}</>}
    </>
  );
};
