import React, { FC } from "react";
import { CardCombination } from "../../../types/api";

interface Props {
  cards: CardCombination;
}

export const DesiredAmount: FC<Props> = ({ cards }) => {
  return (
    <>
      <p>Votre montant est composé des cartes suivantes :</p>
      {cards.cards.map((cardValue, index) => (
        <div key={`cardValue-${index}`}>- {cardValue} €</div>
      ))}
    </>
  );
};
