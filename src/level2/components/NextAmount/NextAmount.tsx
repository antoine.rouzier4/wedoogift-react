import React, { FC } from "react";
import { ApiCards } from "../../../types/api";

interface Props {
  data: ApiCards;
  queryWithAmount: (newAmount: string) => void;
}

export const NextAmount: FC<Props> = ({ data, queryWithAmount }) => {
  const { ceil, floor } = data;

  const onPressNextAmount = () => {
    if (!ceil) return;
    if (floor && ceil.value > floor.value) {
      queryWithAmount(ceil.value.toString());
    } else {
      queryWithAmount(`${ceil.value + 1}`);
    }
  };

  const onPressPreviousAmount = () => {
    if (!floor) return;
    if (ceil && floor.value < ceil.value) {
      queryWithAmount(floor.value.toString());
    } else {
      queryWithAmount(`${floor.value - 1}`);
    }
  };

  return (
    <>
      <button type="button" onClick={onPressNextAmount}>
        Montant suivant
      </button>
      <button type="button" onClick={onPressPreviousAmount}>
        Montant précédent
      </button>
    </>
  );
};
