import { CardCombination } from "../../../types/api";

export type ChooseAmountProp = {
  amount: CardCombination;
  onPress: () => void;
};
