import React, { FC } from "react";
import { ChooseAmountProp } from "./types";

interface Props {
  amount1: ChooseAmountProp;
  amount2: ChooseAmountProp;
}

export const ChooseAmount: FC<Props> = ({ amount1, amount2 }) => {
  const renderAmount = (amount: ChooseAmountProp) => {
    const { onPress } = amount;
    const { value, cards } = amount.amount;
    return (
      <button onClick={onPress}>
        {value} €
        {cards.length > 1 && (
          <>
            {" - ("}
            {cards.map(
              (card, index) =>
                `${card} €${index + 1 < cards.length ? " + " : ""}`
            )}
            )
          </>
        )}
      </button>
    );
  };

  return (
    <>
      <p>Choisissez le montant que vous désirez :</p>
      {renderAmount(amount1)}
      {renderAmount(amount2)}
    </>
  );
};
