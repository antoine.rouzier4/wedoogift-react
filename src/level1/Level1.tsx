import React, { FC, useState } from "react";
import { InputContainer, SubmitButtonContainer } from "./styled/Level1";
import { AxiosInstance } from "../constants/api";
import { ApiCards, CardCombination } from "../types/api";
import { ChooseAmount, DesiredAmount } from "./components/index";

interface Props {
  shopId: number;
}

export const Level1: FC<Props> = ({ shopId }) => {
  const [amount, setAmount] = useState<string>("");
  const [data, setData] = useState<ApiCards | undefined>(undefined);

  const handleSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
    AxiosInstance.get(`shop/${shopId}/search-combination`, {
      params: { amount },
    }).then((res) => setData(res.data));
  };

  const onAmountChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAmount(e.target.value);
  };

  const queryWithCardCombination = (cardCombination?: CardCombination) => {
    const newAmount = cardCombination?.value?.toString() ?? "";
    AxiosInstance.get(`shop/${shopId}/search-combination`, {
      params: { amount: newAmount },
    }).then((res) => {
      setData(res.data);
      setAmount(newAmount);
    });
  };

  const renderData = () => {
    if (data && data?.equal) {
      return <DesiredAmount cards={data.equal}></DesiredAmount>;
    }
    if (data?.ceil && data?.floor) {
      return (
        <ChooseAmount
          amount1={{
            amount: data.ceil,
            onPress: () => queryWithCardCombination(data?.ceil),
          }}
          amount2={{
            amount: data.floor,
            onPress: () => queryWithCardCombination(data?.floor),
          }}
        ></ChooseAmount>
      );
    }
    const cardCombination = data?.ceil ?? data?.floor;
    if (!cardCombination) return;
    queryWithCardCombination(cardCombination);
    return <></>;
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Montant désiré :</label>
        </div>
        <InputContainer>
          <input
            type="number"
            placeholder="60"
            name="amount"
            value={amount}
            onChange={onAmountChange}
          />
        </InputContainer>
        <SubmitButtonContainer>
          <input type="submit" value="Valider" />
        </SubmitButtonContainer>
      </form>
      {data && <>{renderData()}</>}
    </>
  );
};
